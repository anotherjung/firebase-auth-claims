
var admin = require("firebase-admin");

//download your key from console/settings/service-account
var serviceAccount = require("./private/serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://userauth-1018.firebaseio.com"
});


const auth = admin.auth();

//check your console for uid
const uid = 'YwYg7P7g4eOaes94YaFt7RPzdXi2';

const customClaims = {
    admin: true,
    level: 7,
};


(async() => {
    await auth.setCustomUserClaims(uid, customClaims);
    const user = await auth.getUser(uid); 
    console.log('success', user)
    process.exit()
})();
